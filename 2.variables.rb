=begin
  Ruby supported five types of variables: Global variables, class variables, instance variables, local variables and
  constants.
=end

# A constants begin with an uppercase letter, It's scope depend on where it defined, it may not be defined within methods.
# Define a global constant
LANG = 'Ruby'

# Define a global variable
$global_var = 'Global var'

# Class variable must be initialized before the can be used in method definitions. Referencing an uninitialized class
# variable produces an error. Class variables shared among descendants

# define a function
def p(str)
  puts "-------- #str ---------"
end

# array
# p('Array')
ary = ['david', 2, "peter", ]
ary.each do |i|
  puts i
end

# hashes
hsh = colors = {"red" => 0xf00, "green" => 0x0f0, "blue" => 0x00f}
hsh.each do |key, value|
  print key, " is ", value, "\n"
end

# range
(1..10) .each do |n|
  print n, ' '
end

# : and ::
GLOBAL_CONST = 5
module A
  GLOBAL_COST = 'Class const'
  ::GLOBAL_CONST += 1
  GLOBAL_CONST = 'new class cost'
end
puts GLOBAL_CONST
puts A::GLOBAL_CONST

CONST = 'out there'
class Inside_one
  CONST = proc {' in there'}
  def where_is_my_CONST
    ::CONST + ' inside one'
  end
end

class Inside_two
  CONST = ' inside two'
  def where_is_my_CONST
    CONST
  end
end

puts Inside_one.new.where_is_my_CONST
puts Inside_two.new.where_is_my_CONST
puts Object::CONST + Inside_two::CONST
puts Inside_two::CONST + CONST
puts Inside_one::CONST
puts Inside_one::CONST.call + Inside_two::CONST