# if..else
if 1 == true
  puts "1 equal to true"
else
  puts '1 is not equal to true'
end

# while
$a = 1
while $a < 3 do
  puts 'Increase $a'
  $a += 1
end

# method
def add(a=2, b=5)
  sum = a + b
  if sum < 10
    return sum, 'one digit'
  else
    return sum, 'two digit'
  end
end

# call method
puts add
puts add 3, 5

# block
def test
  puts 'start test'
  puts 'yield 1'
  yield 1
  puts 'yield 2'
  yield 2
  puts 'end test'
end

test {|i| puts "this block will invoked by yield keyword and param is #{i}"}