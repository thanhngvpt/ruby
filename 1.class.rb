=begin
Ruby is a perfect object oriented programming language. The features of the object oriented programming language
include: Data encapsulation,  data abstraction, polymorphism, inheritance.

Variables in a ruby class:
  1. Local variables: Defined in a method it isn't  available outside the method, begin with a lowercase letter or _
  2. Instance variables: Available across methods for any particular instance or object, it preceded by the @ sign and
    followed by the variable name.
  3. Class variables: Available across different object, it belong  to the class and is a characteristic of a class.
    It preceded by the @@ sign.
  4. Global variables: Available across all classes, it preceded by $ sign.
=end

# Define class
class Customer
  # class variables
  @@no_of_customers = 0
  def initialize(id, name, addr)
    # Instance variables
    @cust_id=id
    @cust_name=name
    @cust_addr=addr
  end
  def display_details
    # If method has no parameter then no need to use brace while definition and usage as this
    puts "Customer id: #@cust_id"
    puts "Customer name: #@cust_name"
    puts "Customer addr: #@cust_addr"
  end
  def total_no_of_customers()
    # We still use brace but it's not to recommended
    @@no_of_customers += 1
    puts "Total number of customers: #@@no_of_customers"
  end
end

# Create objects
cust1 = Customer.new("1", "John", "Wisdom Apartments, Ludhiya")
cust2 = Customer.new("2", "Paul", "Ohio Apartments, Ludhiya")

# Call methods
cust1.display_details()
cust2.total_no_of_customers
